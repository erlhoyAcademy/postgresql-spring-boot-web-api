package erlhoyAcademy.springPostgresqlHibernate.repositories;

import erlhoyAcademy.springPostgresqlHibernate.entities.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
