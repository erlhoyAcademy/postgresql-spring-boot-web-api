package erlhoyAcademy.springPostgresqlHibernate.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name="actors")
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "full_name", nullable = false, length = 100)
    private String fullName;

    @Column(name = "alias", length = 100)
    private String alias;

    @Column(name = "gender", length = 25)
    private String gender;

    @Column(name = "picture_url", length = 200)
    private String pictureURL;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name="actor_movie",
            joinColumns = {@JoinColumn(name="actor_id")},
            inverseJoinColumns = {@JoinColumn(name="movie_id")}
    )
    private Set<Movie> movies = new HashSet<>();


    @JsonGetter("movies")
    public List<String> getTheMovies() {
        if (movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "api/movie/" + movie.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }


    public Character() {

    }

    public Character(String fullName, String alias, String gender, String pictureURL) {
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.pictureURL = pictureURL;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
