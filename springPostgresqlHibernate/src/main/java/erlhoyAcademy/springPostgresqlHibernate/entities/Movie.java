package erlhoyAcademy.springPostgresqlHibernate.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "movies")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "movie_title", nullable = false, length = 75)
    private String movieTitle;

    @Column(name = "genre", nullable = false, length = 200) // genre can be a string of comma-seperated genres
    private String genre;

    @Column(name = "release_year", length = 4)
    private String releaseYear;

    @Column(name = "director", length = 50)
    private String director;

    @Column(name = "picture_url", length = 250)
    private String pictureUrl;

    @Column(name = "trailer_url", length = 250)
    private String trailerUrl;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "movies")
    private Set<Character> actors = new HashSet<>();

    @JsonGetter("actors")
    public List<String> getTheActors() {
        if (actors != null) {
            return actors.stream()
                    .map(actor -> {
                        return "api/actor/" + actor.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "franchise_id")
    private Franchise franchise = new Franchise();

    @JsonGetter("franchise")
    public String getTheFranchise() {
        if (franchise != null) {
            return "api/franchise/" + franchise.getId();
        }
        return null;
    }

    public Movie() {

    }

    public Movie(String movieTitle, String genre, String releaseYear, String director, String pictureUrl, String trailerUrl) {
        this.movieTitle = movieTitle;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.pictureUrl = pictureUrl;
        this.trailerUrl = trailerUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public Set<Character> getActors() {
        return actors;
    }

    public void setActors(Set<Character> actors) {
        this.actors = actors;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }
}
