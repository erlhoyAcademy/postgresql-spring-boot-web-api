package erlhoyAcademy.springPostgresqlHibernate.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "franchises")
public class Franchise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "franchise_name", nullable = false, length = 50)
    private String franchiseName;

    @Column(name = "description", length = 300)
    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "franchise")
    private Set<Movie> movies = new HashSet<>();

    @JsonGetter("movies")
    public List<String> getTheMovies() {
        if (movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "api/movie/" + movie.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public Franchise() {

    }

    public Franchise(String franchiseName, String description) {
        this.franchiseName = franchiseName;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFranchiseName() {
        return franchiseName;
    }

    public void setFranchiseName(String franchiseName) {
        this.franchiseName = franchiseName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
