package erlhoyAcademy.springPostgresqlHibernate;

import erlhoyAcademy.springPostgresqlHibernate.entities.Character;
import erlhoyAcademy.springPostgresqlHibernate.entities.Franchise;
import erlhoyAcademy.springPostgresqlHibernate.entities.Movie;
import erlhoyAcademy.springPostgresqlHibernate.repositories.CharacterRepository;
import erlhoyAcademy.springPostgresqlHibernate.repositories.FranchiseRepository;
import erlhoyAcademy.springPostgresqlHibernate.repositories.MovieRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class WebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebApiApplication.class, args);
	}


	@Bean
	public ApplicationRunner dummyData(CharacterRepository cr, MovieRepository mr, FranchiseRepository fr) {
		return (args) -> {

			Character bale = new Character("Christian Bale", "Batman", "male", "https://sv.wikipedia.org/wiki/Christian_Bale");
			cr.save(bale);

			Movie batman1 = new Movie(
					"Batman Begins",
					"Action",
					"2005",
					"Matt Reeves",
					"https://www.imdb.com/title/tt0372784/",
					"https://www.imdb.com/title/tt0372784/");

			mr.save(batman1);

			// add references to each other (movie and character)
			for (Character actor : cr.findAll()) {
				actor.getMovies().add(batman1);
				batman1.getActors().add(actor);
			}

			Franchise batmanFranchise = new Franchise("Batman franchise", "The franchise including batman movies");
			fr.save(batmanFranchise);

			batmanFranchise.getMovies().add(batman1);
			batman1.setFranchise(batmanFranchise);



		};
	}
}



