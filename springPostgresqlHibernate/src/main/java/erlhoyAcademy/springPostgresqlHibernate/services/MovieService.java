package erlhoyAcademy.springPostgresqlHibernate.services;

import erlhoyAcademy.springPostgresqlHibernate.entities.Movie;
import erlhoyAcademy.springPostgresqlHibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    public Optional<Movie> getMovieById(long movieId) {
        return movieRepository.findById(movieId);
    }

    public void saveOrUpdateMovie(Movie movie) {
        movieRepository.save(movie);
    }

    public void deleteMovieById(long movieId) {
        movieRepository.deleteById(movieId);
    }
}
