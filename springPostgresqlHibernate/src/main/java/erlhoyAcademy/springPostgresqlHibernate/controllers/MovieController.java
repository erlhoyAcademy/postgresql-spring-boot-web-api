package erlhoyAcademy.springPostgresqlHibernate.controllers;

import erlhoyAcademy.springPostgresqlHibernate.entities.Movie;
import erlhoyAcademy.springPostgresqlHibernate.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping("/all")
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieService.getAllMovies();
        HttpStatus response = HttpStatus.OK;
        return new ResponseEntity<>(movies, response);
    }

    @GetMapping("/{movieId}")
    public ResponseEntity<Optional<Movie>> getMovieById(@PathVariable long movieId) {
        Optional<Movie> movie = movieService.getMovieById(movieId);
        HttpStatus response = HttpStatus.OK;
        return new ResponseEntity<>(movie, response);
    }

    @PostMapping("")
    public ResponseEntity<Movie> createOrUpdateMovie(@RequestBody Movie movie) {
        movieService.saveOrUpdateMovie(movie);
        HttpStatus response = HttpStatus.CREATED;
        return new ResponseEntity<Movie>(movie, response);
    }

    @DeleteMapping("/{movieId}")
    public ResponseEntity<Long> deleteMovieById(@PathVariable long movieId) {
        movieService.deleteMovieById(movieId);
        HttpStatus response = HttpStatus.OK;
        return new ResponseEntity<>(movieId, response);
    }
}
