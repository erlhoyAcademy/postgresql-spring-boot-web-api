package erlhoyAcademy.springPostgresqlHibernate.dummyData;

import erlhoyAcademy.springPostgresqlHibernate.entities.Character;
import erlhoyAcademy.springPostgresqlHibernate.entities.Franchise;
import erlhoyAcademy.springPostgresqlHibernate.entities.Movie;
import erlhoyAcademy.springPostgresqlHibernate.repositories.CharacterRepository;
import erlhoyAcademy.springPostgresqlHibernate.repositories.FranchiseRepository;
import erlhoyAcademy.springPostgresqlHibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class MovieDummyData implements ApplicationRunner {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {


        // Franchise
        Franchise batmanFranchise = new Franchise("Batman franchise", "The franchise including batman movies");
        franchiseRepository.save(batmanFranchise);


        // Characters (Actors)
        Character bale = new Character("Christian Bale", "Batman", "male", "https://sv.wikipedia.org/wiki/Christian_Bale");
        Character joker = new Character("Heath Ledger", "The Joker", "male", "https://www.imdb.com/title/tt0372784/");

        Set<Character> actors = new HashSet<>();
        actors.add(bale);
        actors.add(joker);
        characterRepository.saveAll(actors);

        // Movies
        // Movie: Batman Begin
        Movie batman1 = new Movie(
                "Batman Begins",
                "Action, Adventure",
                "2005",
                "Matt Reeves",
                "https://www.imdb.com/title/tt0372784/",
                "https://www.imdb.com/title/tt0372784/");

        // Movie: The Dark Knight
        Movie batman2 = new Movie(
                "The Dark Knight",
                "Action, Adventure",
                "2008",
                "Matt Reeves",
                "https://www.imdb.com/title/tt0468569/",
                "https://www.imdb.com/title/tt0468569/");

        // Movie: The Dark Knight Rises
        Movie batman3 = new Movie(
                "The Dark Knight Rises",
                "Action, Adventure",
                "2012",
                "Matt Reeves",
                "https://www.imdb.com/title/tt1345836/",
                "https://www.imdb.com/title/tt1345836/");


        // Add actor-relation to movies
        batman1.setActors(Set.of(bale, joker));
        batman2.setActors(Set.of(bale, joker));
        batman3.setActors(Set.of(bale, joker));

        // Add franchise-relation to movies
        batman1.setFranchise(batmanFranchise);
        batman2.setFranchise(batmanFranchise);
        batman3.setFranchise(batmanFranchise);

        Set<Movie> movies = new HashSet<>();
        movies.add(batman1);
        movies.add(batman2);
        movies.add(batman3);
        movieRepository.saveAll(movies);
    }
}
